<?php

namespace app\exam\controller;
use think\Controller;
use app\exam\model\Subject;
use app\exam\model\Paper;
use think\Request;

class Onlinetest extends Controller{


    //首页
    public function index(){
        $subjectModel = new Subject();
        $data = $subjectModel->getsubject();
        $this->assign('subjectList',$data);
        return $this->fetch('index');
    }
    //试卷列表
    public function paperlist(Request $request){
        $id = $request->GET('id');
        $subject = $request->GET('subject');
        $subjectModel = new paper();
        $data =$subjectModel->getpapern($id);
        $this->assign('pinfo',$data);
        $this->assign('subject',$subject);
        return $this->fetch('paperlist');
    }





}