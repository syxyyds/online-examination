<?php

namespace app\exam\controller;
use think\Controller;
use think\Request;
use app\exam\model\User as userModel;

class User extends Controller{

    //用户列表管理
    public function index(){
        return $this->fetch('user');
    }

    public function user()
    {

        //获得数据总数
        $userModel = new userModel();
        $data = $userModel->getUser();
        
        $allcount = count($data);
        
        //获取传递的分页参数
        $page=request()->param('page');
        $limit=request()->param('limit');
        $start=$limit*($page-1);

        //分页查询
        $userpage = $userModel->getpage($start,$limit);
        $res = [
                    'code'=>0,
                    'msg'=>'返回成功',
                    'count'=>$allcount,
                    'data'=>$userpage
                ];
        return json($res);
    }

    //删除用户
    public function deluser(Request $request){
        if($request->isAjax()){
            $id = $request->param('id');
            $userModel = new userModel();
            $res = $userModel->deluser($id);
            return $res;
        }else{
            return false;
        }
    }














}