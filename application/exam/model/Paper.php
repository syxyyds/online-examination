<?php


namespace app\exam\model;

use think\Model;
use think\Db;

class Paper extends Model{

    //获取试卷列表
    public function getpaper(){
        return $this->select();
    }
    //获取列表页码
    public function getpage($start,$limit){
        return $this->order('id','asc')->limit($start,$limit)->select();
    }
 //增加试卷
    public function addp($pname,$subject){
        $data =Db::name('question')->where(["subject"=>$subject,"type"=>0])->orderRand()->limit(20)->select();
        $list ='';
        foreach($data as $item=>$vals){
            $list.=$vals['id'].',';
        }
        $pddata = DB::name('question')->where(["subject"=>$subject,"type"=>1])->orderRand()->limit(5)->select();
        foreach($pddata as $item=>$vals){
            $list.=$vals["id"].',';
        }
        $list = rtrim($list, ',');

        return $this->insert(['pname'=>$pname,'testq'=>$list,'subject'=>$subject]);
    }
    //修改试卷
    public function updp($pname,$subject,$id,$testq){
        return $this->where('id',$id)->update(['pname'=>$pname,'subject'=>$subject,'testq'=>$testq]);
    }
    //删除试卷
    public function deleltp($id){
        return $this->where('id',$id)->delete();
    }
    //获取试卷详情
    public function getdetailp($id){
        $testq = $this->where('id',$id)->value('testq');
        $qidList = explode(',',$testq); 
        $list =[];
        foreach($qidList as $item=>$qid){
            $list[] = DB::name('question')->where('id',$qid)->find();
        }
        return $list;
    }
    //获取试卷名
    public function getpname($id){
        return $this->where('id',$id)->value('pname');
    }

    public function getpapern($id){
        $subject = Db::name('subject')->where('id',$id)->value('subject');
        return $this->where('subject',$subject)->select();

    }
    //试卷记录表
    public function spaper($pname,$subject,$userid,$ctime,$answe,$ans){
        $count =0;
        $whether = 0;
        $qidlist = [];
        foreach($answe as $item=>$val){
          $question  =  Db::name('question')->where('id',$item)->find();
          
          if($question["answer"] === $val){
              $count+=1;
              $whether = 1;
          }  else{
            $whether = 0;
          }
          $list = [
            "qid"=>$item,
            "uanswer"=>$val,
            "whether"=>$whether,
            "content"=>$question["content"],
            "sa"=>$question["sa"],
            "sb"=>$question["sb"],
            "sc"=>$question["sc"],
            "sd"=>$question["sd"],
            "answer"=>$question["answer"]
          ];
          $qid =  DB::name('squestion')->insertGetId($list);
         array_push($qidlist,$qid);
        }
        $score = $count*4;
        
        $qidstr = implode(',',$qidlist);
        $res = Db::name('spaper')->insert(["subject"=>$subject,"userid"=>$userid,"ctime"=>$ctime,
        "qidstr"=>$qidstr,"pname"=>$pname,"score"=>$score]);
        return $res;




    }


     //获取试卷列表
     public function getspaper($id){
        return DB::name('spaper')->where('userid',$id)->select();
    }
    //获取列表页码
    public function getspage($start,$limit,$id){
        return DB::name('spaper')->where('userid',$id)->order('id','asc')->limit($start,$limit)->select();
    }


    public function getupaper($id){
        $data = DB::name('spaper')->where('id',$id)->value('qidstr');
        $datalist =  explode(',',$data); 
        $list =[];
        foreach($datalist as $item=>$qid){
            $list[] = DB::name('squestion')->where('id',$qid)->find();
        }
        return $list;
        
    }
    //管理历史试卷列表赋值
    public function getadspaper(){
        return DB::name('spaper')->select();
    }

    public function getadspage($start,$limit){
        return DB::name('spaper')->order('id','asc')->limit($start,$limit)->select();
    }

    public function deleltadsp($id){
        return DB::name('spaper')->where('id',$id)->delete();
    }






}